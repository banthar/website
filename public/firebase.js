const imageDict = {
    "AGUSIA": "aga.jpg",
    "ANIA_G": "ania.jpg",
    "MAKOZAKI": "makozaki.jpeg",
    "GREENHULK": "wlodek.jpeg",
    "BARTEK_N": "bartlomiej.jpg",
    "DAVID": "david.jpg",
    "DOCDANEEKA": "olek.jpg",
    "KRWAWOBRODY": "praszmo.jpeg",
    "LUKASZ______": "lukasz.jpg",
    "M3M3K": "przemek.jpg",
    "MADZIA": "magda.jpg",
    "MICHAL_A": "anglart.jpg",
    "MICHAL_M": "mocek.jpg",
    "MICHAŁ_R": "michalrapacz.jpg",
    "NEFRO85": "mariusz.jpeg",
    "PAWELJ": "pawel.jpg",
    "RACIK": "racik.jpg",
    "MICHAL_Z": "Michal_Z.jpeg",
    "DROPS": "drops.jpg",
}

const quoteDict = {
    "AGUSIA": ["Najbardziej jara mnie sauna"],
    "ANIA_G": ["Nie mogę grać w squasha bo studia"],
    "MAKOZAKI": ["Każda cebula to dobra cebula"],
    "GREENHULK": ["Ale afrykańskie upały"],
    "BARTEK_N": ["Cześć"],
    "DAVID": ["Ała"],
    "DOCDANEEKA": ["Dzisiaj jest dobra zupa", "Dzisiaj nie ma dobrej zupy"],
    "KRWAWOBRODY": ["Jakie jest twoje pytanie?", "Zrób tak, żeby było dobrze", ],
    "LUKASZ______": ["Ja chcę odbijać, a nie grać"],
    "M3M3K": ["Może zagramy niebieską piłką?"],
    "MADZIA": ["Gramy do końca..."],
    "MICHAL_A": ["Na moje oko nie weszła"],
    "MICHAL_M": ["Z kolegami najlepiej"],
    "MICHAŁ_R": ["Klikajcie Accept->Send Response", "Sauna - srauna", "iPad - iSrad"],
    "NEFRO85": ["Lepiej wygrać, niż przegrać", "Czasem mam problemy z orientacją"],
    "PAWELJ": ["Jesteście teamem przez wielkie T","Dzisiaj ryba w sosie porowym"],
    "RACIK": ["Endrju czemu zabierasz mi ludzi z pegazusa", "Kto chce cukierka?"],
    "SIKOR": ["Za tydzień na pewno będę"],
    "MICHAL_Z": ["Znowu ostatni"],
    "DROPS": ["Fajnie się grało"]
}

const config = {
    apiKey: "AIzaSyB35ElGEVMRokMOXjso2Wpvzh3epRQomqk",
    authDomain: "challongeproject-4ebbe.firebaseapp.com",
    databaseURL: "https://challongeproject-4ebbe.firebaseio.com",
    projectId: "challongeproject-4ebbe",
    storageBucket: "challongeproject-4ebbe.appspot.com",
    messagingSenderId: "198324419356"
};

var completedTournaments = null;

function setUpSeasonMenu(completedTournaments, ongoingTournaments) {
    let seasonMenu = document.getElementById('seasonMenu');
    for (name in completedTournaments) {
        let button = document.createElement("button");
        button.innerText = name;
        button.className = "seasonButton";
        button.addEventListener("click", function () {
            $('#fullpage').fullpage.destroy('all');
            $("#myMenu").empty();
            setUpPage(this.innerText);
        });
        seasonMenu.appendChild(button);
    }
    if (ongoingTournaments){
        let button = document.createElement("button");
        button.onclick = function () {
            window.open(ongoingTournaments[0].sign_up_url, "_blank");
        }
        button.innerText = "Register now for: " + ongoingTournaments[0].name;
        button.className = "seasonButton";
        seasonMenu.appendChild(button);
    }
}

function loadDatabaseInfo() {
    firebase.initializeApp(config);
    const database = firebase.database();
    database.ref().once('value').then(function (snapshot) {
        let ongoingTournaments = snapshot.val().ongoing;
        completedTournaments = snapshot.val().completed;
        setUpSeasonMenu(completedTournaments, ongoingTournaments);
        setUpPage("SPRING_2018");
    });
};

function createTournamentTable(player) {
    let th1 = document.createElement("th");
    th1.appendChild(document.createTextNode("Tournament"));
    let th2 = document.createElement("th");
    th2.appendChild(document.createTextNode("Rank"));
    let tr = document.createElement("tr");
    tr.appendChild(th1);
    tr.appendChild(th2);
    let tournamentTable = document.createElement("table");
    tournamentTable.className = 'tournament-table';
    tournamentTable.appendChild(tr);

    let tournaments = player[1].player_tournaments;
    tournaments.forEach(function (tournament, index) {
        let name = tournament[0];
        let finalRank = tournament[1];
        let td1 = document.createElement("td");
        td1.appendChild(document.createTextNode(name));
        let td2 = document.createElement("td");
        td2.appendChild(document.createTextNode(finalRank));
        td2.align = 'center';
        let tr = document.createElement("tr");
        tr.appendChild(td1);
        tr.appendChild(td2);
        tournamentTable.appendChild(tr);
    });
    return tournamentTable;
}

function getQuote(key) {
    if (quoteDict.hasOwnProperty(key.toUpperCase())) {
        let quotes = quoteDict[key.toUpperCase()];
        return "\"" + quotes[Math.floor(Math.random() * quotes.length)] + "\"";
    } else {
        return ""
    }
}

function setUpPage(seasonName) {
    const elementsList = document.querySelector("#fullpage")
    elementsList.innerHTML = "";
    const menu = document.querySelector("#myMenu");
    const playerAnchors = [];
    let players = completedTournaments[seasonName];

    players.forEach(function (player, index) {
        let key = player[0];
        const div = document.createElement("div");
        if (imageDict.hasOwnProperty(key.toUpperCase())) {
            div.style.backgroundImage = "url(" + imageDict[key.toUpperCase()] + ")";
        }
        div.classList.add("section");
        const h2 = document.createElement("h2");
        h2.classList.add("element-title");
        h2.innerText = player[1].display_name + ": " + player[1].season_score;
        const innerDiv = document.createElement("div");
        innerDiv.innerText = getQuote(key);
        innerDiv.classList.add("element-text");
        div.appendChild(h2);
        div.appendChild(innerDiv);

        let tournamentTable = createTournamentTable(player);
        h2.appendChild(tournamentTable);

        elementsList.appendChild(div);

        const li = document.createElement("li");
        const a = document.createElement("a");
        li.dataset.menuanchor = "player" + index;
        a.href = "#player" + index;
        playerAnchors.push("player" + index);
        a.innerText = player[1].display_name;
        const score = document.createElement("span");
        score.className = "score";
        score.innerText = player[1].season_score;
        let playerAvatarInfo = player[1].avatar;

        const avatar = document.createElement("img");
        if (playerAvatarInfo) {
            avatar.src = playerAvatarInfo.replace(/^(\/\/)/, "http://");
        } else {
            avatar.src = "//:0";
        }
        avatar.className = "avatar";
        li.appendChild(avatar);
        a.appendChild(score);
        li.appendChild(a);
        menu.appendChild(li);
    });

    $('#fullpage').fullpage({
        anchors: playerAnchors,
        menu: '#myMenu',
        dragAndMove: true
    });
}

$(document).ready(function () {
    loadDatabaseInfo();
})
